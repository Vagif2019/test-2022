<?php

final class Email
{
    private $value;

    public function __construct($email)
    {
        $this->setEmail($email);
    }

    public function getEmail()
    {
        return $this->value;
    }
    private function setEmail($email){
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new InvalidArgumentException('Email is invalid');
        }
        $this->value = $email;
    }
}




final class Password{
    private $password;
    public function __construct($password){
        $this->setPassword($password);
    }
    private function setPassword($password)
    {
        if(mb_strlen($password) < 8) {
            throw new InvalidArgumentException('pass invalid');
        }
            $this->password = $password;
        }


    public function getPassword()
    {
        return $this->password;
    }

    public function isEqual(Password $password){
        if ($this->password === $password->getPassword()){
            return true;
            }
        return false;
    }
}
$password1 = new Password('12345678');
$password2 = new Password('12345678');
$email = new Email('some@gmail.com');


if ($password1->isEqual($password2)){
    echo "PassOK!"  . "<br>";
}
echo $email->getEmail();
